import React, {useState} from 'react';

import { When } from 'react-if';

import Nav from 'react-bootstrap/Nav'
import Navbar from 'react-bootstrap/Navbar'
import Alert from 'react-bootstrap/Alert'
import Container from 'react-bootstrap/Container'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
import ListGroup from 'react-bootstrap/ListGroup'
import Form from 'react-bootstrap/Form'

const defaultLookupTable = Buffer.from('4959433db5bf6da347534f6165e371e9677f02030badb3892b2f35c16b8b959711e5a70deff1050783fb9d3bc5c713171d1f2529d3df', 'hex');

const samples = [
  'EDHNFGBILOINGGIGFDHHFKFIGNNIHDMFHCEEABDGBAIHKPKCCMBLCPOJHBKGJGKABDMJLKCGPFMH:camera',
  'EBGDEOBOKGICGAIAFFGNEAFMGPMHGFNDGNFLBHDKADJGLBKFDOBEDGPEHAKBIJLMBG:camera',
  'EIHGFNBBKAIEGEJLELHBFEELGDNNGBMIHFFMAPDMAAJEKEKPDLAJDPPKGALBIBLAAENOKNDAOFNFBICNIF:SHIX',
];

function ab2str(buf) {
  return String.fromCharCode.apply(null, new Uint16Array(buf));
}

function decode(sample, lookupTable) {
  const [encoded, name = 'name not included'] = sample.split(':')
  let output = Buffer.alloc(encoded.length/2);

  for (var i = 0; i <= sample.length/2; i++) {
    var z = 0x39 // 57 // '9'

    for (var j = 0; j < i; j++) {
      z = z ^ output[j];
    }

    const x = (sample.charCodeAt(i * 2 + 1) - 'A'.charCodeAt())
    const y = (sample.charCodeAt(i * 2) - 'A'.charCodeAt()) * 0x10
    output[i] = z ^ lookupTable[i % lookupTable.length] ^ x + y
  }

  return {name, output: ab2str(output)}
}

function App() {
  const [name, setName] = useState('');
  const [output, setOutput] = useState('');

  const handleSubmit = async (event) => {
    event.preventDefault();
    const { target } = event;
    const data = new FormData(target);
    const encoded = data.get('encoded');
    const lookupTable = data.get('lookupTable');
    const { name, output } = decode(encoded, Buffer.from(lookupTable, 'hex'));
    setName(name);
    setOutput(output);
  }

  return (
    <>
      <Navbar>
        <Navbar.Brand>PPPP_DecodeString</Navbar.Brand>
        <Nav>
        <Nav.Link href="https://gitlab.com/bettse/pppp_decodestring/">
          Sourcecode (GitLab)
        </Nav.Link>
        </Nav>
      </Navbar>
      <Container>
        <Row>
          <Col lg={12}>
            <Card>
              <Card.Header>PPPP_DecodeString</Card.Header>
              <Card.Body>
                <Card.Text>Decode ascii string used in ip cameras to get IP address list</Card.Text>
                <Form onSubmit={handleSubmit} >
                  <Form.Label>Lookup table(hex)</Form.Label>
                  <Form.Control name="lookupTable" defaultValue={defaultLookupTable.toString('hex')} />
                  <Form.Label>Encoded String(ascii)</Form.Label>
                  <Form.Control name="encoded" placeholder="" />
                  <Button type="submit">decode</Button>
                </Form>
              </Card.Body>
            </Card>
          </Col>
        </Row>

        <When condition={name.length > 0}>
          <Row>
            <Col lg={12}>
              <Card>
                <Card.Header>Result</Card.Header>
                <Card.Body>
                  <Alert variant="info">
                    {name}
                  </Alert>
                  <Alert variant="success">
                    {output}
                  </Alert>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </When>

        <Row>
          <Col lg={12}>
            <Card>
              <Card.Header>Samples</Card.Header>
              <Card.Body>
                <ListGroup variant="flush">
                  {samples.map((sample, index) => {
                    const { name, output } = decode(sample, defaultLookupTable)
                    return (
                      <ListGroup.Item key={index}>
                        <Alert variant="primary">
                          {sample}
                        </Alert>
                        <Alert variant="info">
                          {name}
                        </Alert>
                        <Alert variant="success">
                          {output}
                        </Alert>
                      </ListGroup.Item>
                    );
                  })}
                </ListGroup>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
      <footer className="page-footer font-small pt-5 mt-5 mx-auto">
        <Container fluid className="text-center">
          <Row noGutters>
            <Col>
            <small className="text-muted">
              Inpired by <a href="https://github.com/nosoop/pppp_api">nosoop/pppp_api</a>
            </small>
            </Col>
            <Col>
            <small className="text-muted">
              Hosted with <a href="https://www.netlify.com/">Netlify</a>
            </small>
            </Col>
          </Row>
        </Container>
      </footer>
    </>
  );
}

export default App;
